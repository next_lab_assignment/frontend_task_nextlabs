
//  Section A Task 1 Modal Pop Up 
let orderDetailsPopUp = document.querySelectorAll(".orderDetailsPopUp");
let modal = document.getElementById("myModal");
let selectedPrice = document.getElementById("selectedPrice");
let form = document.forms.orderDetails; 

orderDetailsPopUp.forEach(function (button) {
  button.addEventListener("click", function () {
    
let card = button.closest(".card");
let priceElement = card.querySelector(".pricing-card-title");
let planName = card.querySelector(".card-header h4").textContent.trim();

let priceText = priceElement.textContent.trim();

selectedPrice.textContent = planName+" "+priceText;
    modal.style.display = "block";
  });
});


let closeButton = modal.querySelector(".close");

closeButton.addEventListener("click", function () {
  modal.style.display = "none";
  form.reset();
});

//form validation
function validateForm(){  
  let name=document.orderDetails.firstname.value.trim();  
  let email=document.orderDetails.email.value.trim();  
  if (name==null || name==""){  
    alert("Name Required");  
    return false;  
  }

  if (email==null || email==""){  
    alert("Email Required");  
    return false;  
  }
}  


//  Section A : Task 2 the slider feature
let userSlider = document.getElementById("userRange");
let sliderValue = document.querySelector(".slider-value");

let trySample1 = document.getElementById("belowTeen");
let trySample2 = document.getElementById("belowTwenty");
let trySample3 = document.getElementById("belowThirty");

let isSliderActive = false;

function checkSliderValue() {
  let sliderVal = userSlider.value;
  sliderValue.textContent = `${sliderVal} Users`;

  if (isSliderActive && sliderVal <= 10) {
    trySample1.style.border = "2px solid blue";
  } else {
    trySample1.style.border = "none";
  }

  if (isSliderActive && sliderVal > 10 && sliderVal <= 20) {
    trySample2.style.border = "2px solid blue";
  } else {
    trySample2.style.border = "none";
  }

  if (isSliderActive && sliderVal > 20) {
    trySample3.style.border = "2px solid blue";
  } else {
    trySample3.style.border = "none";
  }
}


userSlider.addEventListener("mousedown", function () {
  isSliderActive = true;
});


userSlider.addEventListener("mouseup", function () {
  isSliderActive = false;
});


userSlider.addEventListener("input", checkSliderValue);



// Lazy Loading To Avoid Pagination Infinite Scroll 

const loadingSpinner = document.getElementById('loading-spinner');

let quoteContainer = document.querySelector(".quoteContainer");

let currPage=1;
let quotePerPage=10;
let loading = false;


function showLoadingSpinner() {
  loadingSpinner.style.display = 'block';
}


function hideLoadingSpinner() {
  setTimeout(() => {
    loadingSpinner.style.display = 'none';
}, 1000);
}

//Function to fetch the data from api
async function fetchData() {
  try {
    if (loading) return;

    loading = true;
    showLoadingSpinner()
    const response = await fetch("https://jsonplaceholder.typicode.com/posts?_page=${currPage}&_limit=${quotePerPage}");

    const data = await response.json();


    for (let i = 0; i < data.length; i++) {
      let quoteBox = document.createElement("div");
      quoteBox.classList.add("quote-box");
      let textArea = document.createElement("h5");
      textArea.innerText = data[i].title;
      quoteBox.append(textArea);

      quoteContainer.append(quoteBox);
    }
    loading = false;
    hideLoadingSpinner();
    currPage++;
  } catch (error) {
    console.error("Fetch error:", error);
  }
}


function isAtBottom() {
  return window.innerHeight + window.scrollY >= document.body.offsetHeight;
}

// Event listener for scrolling
window.addEventListener('scroll', () => {
  if (isAtBottom()) {
      fetchData();
  }
});

fetchData();
